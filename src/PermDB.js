// @flow

// expects db to be { [TMP]:{...}, [PERM]:{...}, ... }.
// db's PERM object must be JSON round-trip-able,
// whereas the TMP object can hold anything; generally
// things that are re-recated based on the PERM data.

const FORGETTING = "forgetting";
const DBNAME = "___mydb";
const TMP = "tmp";
const PERM = "perm";

function initPermDB( db ) {
    if( TMP in db == false ) {
        db[TMP] = {};
    }
    if( PERM in db == false ) {
        db[PERM] = {};
    }
    return db;
}

function isValidPermDB( db ) {
    return db != null &&
        PERM in db;
}

// hack to support local storage api in either browser or node context.
function initLocalStorage() {
    if( typeof localStorage === "undefined" || localStorage === null ) {
        /* eslint no-global-assign: off */
        /* eslint no-undef: off */
        const LocalStorage = require( "node-localstorage" ).LocalStorage;
        localStorage = new LocalStorage( "./scratch" );
    }
}

function forgetDBOnce() {
    var forgetting = initPermDB( {} );
    setPath( forgetting, PERM, FORGETTING, true );
    reallySaveDB( forgetting );
}

function isForgottenOnceDB() {
    var json = localStorage.getItem( DBNAME );
    var perm = JSON.parse( json );
    return perm && FORGETTING in perm;
}

function clearForgottenOnceDB() {
    reallySaveDB( initPermDB( {} ) );
}

function loadDB() {
    var db = initPermDB( {} );
    if( isForgottenOnceDB() ) {
        clearForgottenOnceDB();
    }
    else {
        loadPerm( db );
    }
    return db;
}

function loadPerm( db ) {
    var json = localStorage.getItem( DBNAME );
    var perm = JSON.parse( json );
    setPath( db, PERM, perm );
}

function saveDB( db ) {
    if( isForgottenOnceDB() == false ) {
        reallySaveDB( db );
    }
}

function reallySaveDB( db ) {
    var json = JSON.stringify( db[PERM] );
    localStorage.setItem( DBNAME, json );
}
