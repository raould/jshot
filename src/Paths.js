// @flow

// getPath( db, path1, path2, path3 );
var getPath = function() {
    var value = undefined;
    if( arguments.length >= 2 ) {
        var cursor = arguments[0];
        for( var i = 1; i < arguments.length; ++i ) {
            var p = arguments[i];
            if( p in cursor == false ) {
                throw "getPath() error not found: " + Array.prototype.slice.call(arguments).join(", ");
            }
            cursor = cursor[p];
        }
        value = cursor;
    }
    return value;
};

// getPathOrDefault( db, path1, path2, path3, default );
var getPathOrDefault = function() {
    var value = undefined;
    if( arguments.length >= 2 ) {
        var cursor = arguments[0];
        var def = arguments[arguments.length-1];
        for( var i = 1; i < arguments.length-1; ++i ) {
            var p = arguments[i];
            if( p in cursor == false ) {
                return def;
            }
            cursor = cursor[p];
        }
        value = cursor;
    }
    return value;
};

// setPath( db, path1, path2, path3, value );
var setPath = function() {
    if( arguments.length >= 3 ) {
        var cursor = arguments[0];
        var value = arguments[arguments.length-1];
        for( var i = 1; i < arguments.length-2; ++i ) {
            var p = arguments[i];
            if( p in cursor == false ) {
                cursor[p] = {};
            }
            cursor = cursor[p];
        }
        var finalKey = arguments[arguments.length-2];
        cursor[finalKey] = value;
    }
};

// setPath( db, path1, path2, path3, fn );
var setPathFn = function() {
    if( arguments.length >= 3 ) {
        var cursor = arguments[0];
        var fn = arguments[arguments.length-1];
        for( var i = 1; i < arguments.length-2; ++i ) {
            var p = arguments[i];
            if( p in cursor == false ) {
                cursor[p] = {};
            }
            cursor = cursor[p];
        }
        var finalKey = arguments[arguments.length-2];
        cursor[finalKey] = fn( cursor[finalKey] );
    }
};

