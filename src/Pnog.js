// todo: somehow enable flow. use modules? hell.

const EXPECTED_VERSION = "0.0.2";
const VERSION = "version"; // todo: support upgrades/merging.
const TYPE = "type";
const ID = "id";
const DATA = "data";
const FN = "fn";
const TYPE_PADDLE = "paddle";
const TYPE_BALL = "ball";
const X = "x";
const Y = "y";
const Z = "z";
const P3 = "p3"; // position.
const T3 = "t3"; // translate.
const R3 = "r3"; // euler rotate.
const S3 = "s3"; // scale.
const CONSTANTS = "constants";
const TYPEINITS = "typeInits";
const ENTITIES = "entities";
const SCENE = "scene";
const TIMESTAMP = "timestamp";
const FRAMENO = "frameno";
const FPS = "fps";
const WW = "ww";
const WH = "wh";
const SEGMENTS = "segments";
const DIAMETER = "diameter";
const WIDTH = "width";
const HEIGHT = "height";
const DEPTH = "depth";
const SUBDIVISIONS = "subdivisions";
const CAMERAID = "camera1";
const LIGHT1ID = "light1";
const SPHERE1ID = "sphere1";
const ANIM1ID = "anim1";

/* global BABYLON */
/* eslint no-console: off */

// ----------------------------------------

function GameConstants() {
    this[FPS] = 30;
    // todo: match: width, heigh in index.html etc.
    this[WW] = 768;
    this[WH] = 1024;
    this[TYPEINITS] = { // type -> custom data for init fn. todo: flow.
        [TYPE_PADDLE]: {
            [WIDTH]: 0.5,
            [HEIGHT]: 1,
            [DEPTH]: 0.25,
        },
        [TYPE_BALL]: {
            [SEGMENTS]: 4,
            [DIAMETER]: 0.25,
            [SUBDIVISIONS]: 2
        }
    };
}

// ----------------------------------------

function Game( db ) {
    this.db = db;
    this.resize = function() {
        var scene = getPath( this.db, TMP, SCENE );
        if( ! scene ) { return; }
        var engine = scene.getEngine();
        if( ! engine ) { return; }
        engine().resize();
    };
    this.render = function() {
        var scene = getPath( this.db, TMP, SCENE );
        if( ! scene ) { return; }
        scene.render();
    };
    this.step = function() {
        setPath( this.db, PERM, TIMESTAMP, new Date().getTime() );
        setPathFn( this.db, PERM, FRAMENO, function( v ) { return (v||0)+1; } );
        gameStepFn( this.db );
        var scene = getPath( this.db, TMP, SCENE );
        if( ! scene ) { return; }
        scene.render();
    };
    this.runRenderLoop = function() {
        var scene = getPath( this.db, TMP, SCENE );
        if( ! scene ) { return; }
        var engine = scene.getEngine();
        if( ! engine ) { return; }
        engine.runRenderLoop( this.step.bind(this) );
    };
}

// ----------------------------------------

function GameDB() { 
    initPermDB( this );
    this[PERM][VERSION] = EXPECTED_VERSION;
    this[PERM][TIMESTAMP] = 0;
    this[PERM][FRAMENO] = 1;
    this[PERM][CONSTANTS] = new GameConstants();
    // todo: how do we want to CRUD entities?
    this[PERM][ENTITIES] = {
        // todo: flow define & enforce types for these data shapes.
        [TYPE_PADDLE]: {
            "paddleLeft": { [P3]: { [X]:-1, [Y]:0, [Z]:0 } },
            "paddleRight": { [P3]: { [X]:1, [Y]:0, [Z]:0 } }
        },
        [TYPE_BALL]: {
            "ball1": { [P3]: { [X]:0, [Y]:0, [Z]:0 } }
        }
    };
}

// ----------------------------------------

function createPaddleMesh( data, db ) {
    let scene = getPath( db, TMP, SCENE );
    let m = BABYLON.MeshBuilder.CreateBox(
        TYPE_PADDLE,
        { width:data[WIDTH], height:data[HEIGHT], depth:data[DEPTH] },
        scene
    );
    m.setEnabled( false );
    return m;
}

function createBallMesh( data, db ) {
    var scene = getPath( db, TMP, SCENE );
    var m = BABYLON.MeshBuilder.CreateSphere(
        TYPE_BALL,
        { segments:data[SEGMENTS], diameter:data[DIAMETER] },
        scene
    );
    m.setEnabled( false );
    return m;
}

function initEntityMeshes( db ) {
    var scene = getPath( db, TMP, SCENE );
    createPaddleMesh( getPath( db, PERM, CONSTANTS, TYPEINITS, TYPE_PADDLE ), db );
    createBallMesh( getPath( db, PERM, CONSTANTS, TYPEINITS, TYPE_BALL ), db );
}

// todo: how do we add/remove them over time?
function initEntities( db ) {
    var scene = getPath( db, TMP, SCENE );
    for( var type in getPath( db, PERM, ENTITIES ) ) {
        var m = scene.getMeshByName( type );
        if( m ) {
            for( var id in getPath( db, PERM, ENTITIES, type) ) {
                var data = getPath( db, PERM, ENTITIES, type, id );
                if( m && data ) {
                    addNewEntity( type, id, data, db );
                }
            }
        }
    }
}

function getNextId( type, db ) {
    for( var id = 0; id < Number.MAX_SAFE_INTEGER; ++id ) {
        var name = type + id;
        var entities = getPath( db, PERM, ENTITIES, type );
        if( name in entities == false ) {
            return id;
        }
    }
    // todo: worry about going past Number.MAX_SAFE_INTEGER?! (or just bugs.)
    return null;
}

// todo: bad that we are relying on babylon mesh to have the right name?
// vs. maintaining the mapping ourselves, yay duplication of effort.
// but it sucks that we have to take the 'type' parameter here so far?
function addNamedEntity( instance, type, id, data, db ) {
    setPath( db, PERM, ENTITIES, type, id, data );
    setPath( db, TMP, ENTITIES, type, id, instance );
    instance.setEnabled( true );
    if( P3 in data ) {
        var p3 = data[P3];
        instance.position = new BABYLON.Vector3( p3[X], p3[Y], p3[Z] );
    }
    // todo: other settings e.g. translate, rotate, scale, etc.
}

function addNewEntity( type, id, data, db ) {
    var m = getPath( db, TMP, SCENE ).getMeshByName( type );
    if( m ) {
        // todo: so far trying to keep instance names == type+id everywhere.
        // haven't put in much to enforce it, but it seems desireable.
        var instance = m.createInstance( type + id );
        addNamedEntity( instance, type, id, data, db );
    }
}

function initBaseScene( canvasid, db ) { // todo: contractually required global thing for index.html to use?
    var canvas = document.getElementById( canvasid );
    var engine = new BABYLON.Engine( canvas, true );
    var scene = new BABYLON.Scene( engine );
    scene.clearColor = new BABYLON.Color3( 0, 1, 0 );

    // todo: move this cruft out to be data driven from the db? yes...
    var camera = new BABYLON.FreeCamera( CAMERAID, new BABYLON.Vector3( 0, 5, -10 ), scene);
    camera.setTarget( BABYLON.Vector3.Zero() );
    camera.attachControl( canvas, false );
    var light = new BABYLON.HemisphericLight( LIGHT1ID, new BABYLON.Vector3( 0, 1, 0 ), scene);
    light.intensity = 0.5;

    setPath( db, TMP, SCENE, scene );
    initEntityMeshes( db );
    initEntities( db );
    return scene;
}

function stepEntities( db ) {
    var scene = getPath( db, TMP, SCENE );
    for( var type in getPath( db, PERM, ENTITIES) ) {
        for( var id in getPath( db, PERM, ENTITIES, type ) ) {
            var name = type + id;
            var m = scene.getMeshByName( name );
            var p3 = getPath( db, PERM, ENTITIES, type, id, P3 );
            if( m && p3 ) {
                m.position = new BABYLON.Vector3( p3[X], p3[Y], p3[Z] );
            }
        }
    }
}

function gameStepFn( db ) {
    stepEntities( db );
}

function isValidDB( db ) {
    return db != null &&
        isValidPermDB( db ) &&
        VERSION in db[PERM] &&
        EXPECTED_VERSION == db[PERM][VERSION];
}

function createGame( window, canvasid, saveFn, db ) {
    // case 1: never had a db before at all
    // so go ahead and create it, then set up babylon (below).
    if( isValidDB( db ) == false ) {
        db = new GameDB();
    }

    // case 2: pre-existing db, but reloaded scripts.
    // case 2a: old db is still ok: have to set up babylon (below).
    // case 2b: old db is crap: we can't tell here actually,
    // the developer has to update index.html to pass in a null db.

    // todo: case 3: pre-existing db, but GameDB() has changed.
    // have to detect that and somehow merge the newer GameDB
    // into the already existing db?!
    // still have to set up babylon (below).

    initBaseScene( canvasid, db );
    var game = new Game( db );

    window.onbeforeunload = function() {
        saveFn( db );
        return "";
    };

    return game;
}
