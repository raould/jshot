LICENSE: GPL v3

DEMO:
+ If you can actually get things set up and running in a browser (or nw.js
if you are able/crazy), then let the app run for a while and see the
sinusoidal motion of the sphere.
+ Leave things running and go edit src/GameDB.js and change
createTypeAMesh() by commenting out the CreateSphere call, and un-commenting
the CreateBox call.
+ Run the build again, 'npm run build'.
+ Pay attention to the moving object which is still a sphere - and then
reload the web page. The whole point is that the motion of the object
doesn't reset or change, only the object's mesh changes from a sphere to a
cube. Uh, yay. It could not be said to be a particularly good demo.

REQUIRES:
+ *** I STRONGLY URGE BEING ON NPM > 3.X (what they call npm@3 or npm@4 etc.)
BECAUSE OTHERWISE ANY INTERRUPTED NPM COMMAND LIKE "NPM INSTALL" WILL
LEAVE YOUR NODE_MODULES DIRECTORY STRUCTURE FUBAR. ***
+ or use yarn instead of npm, I am trying to use yarn now myself fyi.
+ node 4.2.+
+ babel 6.24.+, babel-preset-env, transform-flow-strip-types
+ flow 0.42.+, which always feels confusing to me to install, yay
+ nw.js 0.21.+, probably want the "SDK" version so you can have the dev
debug tools: npm install nw --nwjs_build_type=sdk
+ babylon.js 2.5.+
+ npm-eslint 3.19.+
+ jquery 3.2.+
+ node-localstorage 1.3.+
+ ...and whatever else is refereneced in package.json, index.html
+ ...and whatever else i forgot to mention ha ha sorry. :-(
+ patience of a saint
+ a dash of perdition

RANDOM NOTES:
eslint doesn't actually respect block scopes?! suck!

TODO:
+ figure out how to extract the game framework vs. the per-game unique code.
+ unit tests ha ha.
+ decide what should go into db[TMP] e.g. all the functions too?
+ decide if we're going to use foo[FOOBAR]: everywhere vs. direct field name access foo.foobar (i am leaning toward using [])
+ fix bugs
+ fix todo's in code (there are some big ones)
+ figure out how to statically check db structure cf. path to entity data
+ support reloading code and using saved db
+ support reloading and patchign saved db
+ support checkpointing db, undo, rewind, etc.
+ support pause, step, play. (rewind?)
+ source maps
+ get flow definitions for Babylon and use them? Looks not possible: the Typescript definitions are not said to not be officially maintained and stopped at v2.3.
+ maybe add another layer of indirection so e.g. entities are our own real type, not just apparently an alias for babylon meshes.
+ use "let" everywhere instead of "var".
+ some day get flow + modules of some kind employed.
+ support using uglify? would it make things run faster?

BUGS:
+ canavs isn't 100%?!?!?! i hacked it to be fixed size, whatever!
+ sometimes see "Exceeded 16 live WebGL contexts"?!?!
+ misc nw.js errors/warnings on startup?!?!
